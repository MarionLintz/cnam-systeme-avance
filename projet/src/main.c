/**
 * \file main.c
 * \brief MiniShell source project
 * \author Marion L. & David L.
 * \version 0.1
 * \date 15 novembre 2020
 *
 * MiniShell project
 */
#include "command/ls.h"
#include "interpreter/interpreter.h"
#include "utils/typedef.h"
#include "utils/alias.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#define NB_MAX_STRING 10
#define NB_MAX_CHAR_IN_STRING 100

#define TRUE 1
#define FALSE 0

void sig_handler(){
  write(1, "\n$> ", 4);
}

    /**
 * Binary main loop
 * \return 0 if exit successfully
 */
int main(int argc, char **argv){
  char *buffer = NULL;
  size_t buf_size = 2048;
  Command *command;
  int commandIsSet = FALSE;
  int resEvaluateInput = EXIT_FAILURE;

  initialiazeTabAlias();

  // buffer to store user command
  buffer = (char *)calloc(buf_size, sizeof(char));
  if (buffer == NULL)
  {
    perror("Malloc failure");
    return (EXIT_FAILURE);
  }

  if(argc > 1) {
    command = (Command *)calloc(1, sizeof(Command));
    command->argv = argv+1;
    command->argc = (int *)calloc(1, sizeof(int));
    *command->argc = argc-1;

    commandIsSet = TRUE;
  }
  else{
    write(1, "$> ", 3);
  }


  // read STDIN
  signal(SIGINT, &sig_handler);

  while (commandIsSet == TRUE || getline(&buffer, &buf_size, stdin) > 0)
  {
    if (commandIsSet == FALSE)
    {
      command = (Command *)calloc(1, sizeof(Command));

      command->argv = (char **)calloc(NB_MAX_STRING, sizeof(char *));
      int count = 0;
      while (count < 10)
      {
        command->argv[count] = (char *)calloc(NB_MAX_CHAR_IN_STRING, sizeof(char));
        count++;
      }

      command->argc = (int *)calloc(1, sizeof(int));
      resEvaluateInput = evaluateInput(buffer, command);
    }

    if (resEvaluateInput == EXIT_SUCCESS || commandIsSet == TRUE)
    {
      // execute
      executeInputIfExists(command);
      free(command);
    }
    else
    {
      printf("Erreur [code %d, main.c:50]: une erreur s'est produite lors de la lecture de la ligne\n", commandIsSet);
    }

    if (commandIsSet == TRUE)
    {
      break;
    }
    else{
      write(1, "$> ", 3);
    }

  }

  free(buffer);

  return EXIT_SUCCESS;
}
