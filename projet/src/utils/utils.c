/**
 * \file utils.c
 * \brief Code portion use to group all methods needed to be used everywhere in the project
 * \author Marion L. & David L.
 * \version 0.1
 * \date 15 novembre 2020
 */

#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_usage(char *bin_name, char *usage_syntax, char *usage_params)
{
    dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, usage_syntax, usage_params);
}

int splitBufferToArray(char *buffer, char *delim, char **tab_output, int *size_output)
{
    int partcount = 0;

    char *str = buffer;
    str[strcspn(str, "\n")] = 0; // remove \n from input
    const char* s = (const char*) delim;
    char *token;

    /* get the first token */
    token = strtok(str, s);

    /* walk through other tokens */
    while (token != NULL)
    {
        //printf("debug[interpreter.c:23] : %s\n", token);
        tab_output[partcount++] = token;
        token = strtok(NULL, s);
    }

    *size_output = partcount;
    tab_output[partcount++] = NULL;

    return EXIT_SUCCESS;
}