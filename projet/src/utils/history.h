#ifndef HISTORY_H_
#define HISTORY_H_

#include "../utils/typedef.h"

/**
 * \fn void writeCommandInHistoryFile(Command *command)
 * \brief write user input in a file to historicize
 * 
 * \param command User input to save
 */
void writeCommandInHistoryFile(Command *command);

#endif // HISTORY_H_