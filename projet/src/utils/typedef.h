#ifndef TYPEDEF_H_
#define TYPEDEF_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/**
 * \struct command 
 * \brief Object command.
 * 
 * Command is a tiny object used to manage user input.
 * The input is splited and stored in an dynamic array, also the array size is stored.
 * 
 */
typedef struct command {
    int* argc; /*!< Number of string in input. */
    char** argv; /*!< Array containing input split by space. */
} Command;

/**
 * \struct alias 
 * \brief Object Alias.
 * 
 * Alias is a tiny object used to manage alias create by user.
 * 
 */
typedef struct alias {
    char* alias;
    char* command;
} Alias;

#endif // TYPEDEF_H_
