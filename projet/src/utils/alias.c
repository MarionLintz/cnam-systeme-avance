/**
 * \file alias.c
 * \brief Code portion use to manage alias
 * \author Marion L. & David L.
 * \version 0.1
 * \date 15 novembre 2020
 */
#include "alias.h"

#include "typedef.h"
#include "utils.h"
#include <unistd.h>
#include <string.h>

#define MAX_ALIAS 30

Alias **ptr_aliasTab;
int* numberAlias;

int initialiazeTabAlias(){
    ptr_aliasTab = (Alias **)calloc(MAX_ALIAS, sizeof(Alias*));

    numberAlias = (int *)calloc(1, sizeof(int));
    *numberAlias = 0;
    return EXIT_SUCCESS;
}

int findAlias(char *alias, char **commandFound)
{
    int count = 0;
    while(count < *numberAlias){
        if (strcmp(ptr_aliasTab[count]->alias, alias) == 0)
        {
            strcpy(*commandFound, ptr_aliasTab[count]->command);
            return EXIT_SUCCESS;
        }
        count++;
    }
    return EXIT_FAILURE;
}

int createOrPrintAlias(Command *command){
    if (*command->argc == 1)
    {
        printAlias();
    }
    else{
        char *delim = (char *)calloc(1, sizeof(char));
        char **tab_output = (char **)calloc(2, sizeof(char *));

        tab_output[0] = (char *)calloc(1, sizeof(char));
        tab_output[1] = (char *)calloc(1, sizeof(char));

        int *size_output = (int *)calloc(1, sizeof(int));

        delim = "=";

        splitBufferToArray(command->argv[1], delim, tab_output, size_output);

        if (*size_output == 2)
        {
            printf("%s=%s\n", tab_output[0], tab_output[1]);
            Alias *a = (Alias *)calloc(1, sizeof(Alias));

            char *aliasChar = calloc(1, sizeof(char));
            char *commandChar = calloc(1, sizeof(char));
            strcpy(aliasChar, tab_output[0]);
            strcpy(commandChar, tab_output[1]);

            a->alias = aliasChar;
            a->command = commandChar;

            ptr_aliasTab[(*numberAlias)] = a;
            (*numberAlias)++;
        }

    }

    return EXIT_SUCCESS;
}

int printAlias(){
    int count = 0;
    while (count < (*numberAlias))
    {
        printf("alias %s=%s\n", ptr_aliasTab[count]->alias, ptr_aliasTab[count]->command);
        count++;
    }

    return EXIT_SUCCESS;
}