#ifndef ALIAS_H_
#define ALIAS_H_

#include "typedef.h"

/**
 * \fn int initialiazeTabAlias()
 * \brief Used to initialize tab alias
 * 
 */
int initialiazeTabAlias();

/**
 * \fn int findAlias()
 * \brief Used to browse alias list and find user input
 * 
 * \param alias Alias to found
 * \param commandFound Match command
 * 
 */
int findAlias(char *alias, char **commandFound);

/**
 * \fn int createOrPrintAlias(Alias alias)
 * \brief Used to execute alias command i.e. store a new alias or print all created alias
 * 
 * \param alias Alias to store
 * 
 */
int createOrPrintAlias(Command *command);

/**
 * \fn int printAlias()
 * \brief Used to print all alias
 * 
 * \param alias Alias to store
 * 
 */
int printAlias();

#endif // ALIAS_H_
