#ifndef UTILS_H_
#define UTILS_H_

/**
 * \fn void print_usage(char *bin_name, char *usage_syntax, char *usage_params)
 * \brief Print in a generic way the usage of a command
 * 
 * \param bin_name command name
 * \param usage_syntax syntax to use to use this command
 * \param usage_params params you can use with the command
 */
void print_usage(char *bin_name, char *usage_syntax, char *usage_params);

/**
 * \fn int splitBufferToArray(char *buffer, char *delim, char **tab_output, int *size_output)
 * \brief Split a string by a delimiter into an array
 * 
 * \param buffer String needed to be split
 * \param delim Delimiter for split
 * \param tab_output Pointer to char* tab to store each new string from split
 * \param size_output Pointer to int to store size of tab create with split
 * 
 * \return return 0 if succeed
 */
int splitBufferToArray(char *buffer, char *delim, char **tab_output, int *size_output);

#endif // UTILS_H_