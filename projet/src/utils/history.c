/**
 * \file history.c
 * \brief Code portion use to historicize 
 * \author Marion L. & David L.
 * \version 0.1
 * \date 15 novembre 2020
 */
#include "history.h"

#include <unistd.h>
#include <time.h>

#define FILE_PATH "history.txt"

void writeCommandInHistoryFile(Command *command){
    FILE *f = fopen(FILE_PATH, "a");
    if (f == NULL)
    {
        exit(1);
    }

    time_t T = time(NULL);
    struct tm tm = *localtime(&T);
    int count = 0;

    fprintf(f, "[%02d/%02d/%04dT%02d:%02d:%02d] ", tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
    
    while(count < *command->argc){
        fprintf(f, "%s ", command->argv[count]);
        count++;
    }
    
    fprintf(f, "\n");
    fclose(f);
}