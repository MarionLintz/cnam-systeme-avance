#ifndef BINCOMMAND_H_
#define BINCOMMAND_H_

#include "../utils/typedef.h"

/**
 * \fn int findFileInDirectory(const char* dirPath, const char* filePath)
 * \brief Search a precise file in a precise directory
 * 
 * \param dirPath Directory you have to go through
 * \param filePath File you have to found
 * 
 * \return return 0 if succeed.
 */
int findFileInDirectory(const char* dirPath, const char* filePath);

/**
 * \fn int executeBinCommandInBin(const char* commandPath)
 * \brief Execute a command pass in argument in /bin
 * 
 * \param commandPath Entire path of the command
 * \param command Name of command and argument 
 * 
 * \return return 0 if succeed.
 */
int executeBinCommandInBin(const char* commandPath, Command *command);

#endif // BINCOMMAND_H_
