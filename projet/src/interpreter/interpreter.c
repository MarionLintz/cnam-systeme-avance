/**
 * \file interpreter.c
 * \brief Code portion use to perform command write in MiniSHell
 * \author Marion L. & David L.
 * \version 0.1
 * \date 15 novembre 2020
 */
#include "interpreter.h"

#include "bincommand.h"
#include "../utils/utils.h"
#include "../utils/typedef.h"
#include "../utils/history.h"
#include "../utils/alias.h"
#include "../builtin/builtin.h"

#include "../command/ls.h"
#include "../command/ps.h"
#include "../command/date.h"
#include <sys/wait.h>
#include <string.h>
#include <errno.h>

#define LS_CODE 1
#define PS_CODE 2
#define DATE_CODE 3

#define CD_CODE 100
#define PWD_CODE 99
#define EXIT_CODE 98
#define ECHO_CODE 97

#define NOT_KNOWN -1

#define BIN_PATH "/bin/"
#define COMMAND_DELAY 1
#define MAX_ALIAS 30


int evaluateInput(char *buffer, Command *output)
{
    return splitBufferToArray(buffer, " ", output->argv, output->argc);
}

int executeInputIfExists(Command *command)
{
    int commandCode = NOT_KNOWN;

    int isAlias = replaceCommandIfIsAlias(command);

    if ((commandCode = isBuiltin(command->argv[0])) > NOT_KNOWN)
    {
        writeCommandInHistoryFile(command);
        return executeBuiltIn(command, commandCode);
    }
    else if ((commandCode = isProgram(command->argv[0])) > NOT_KNOWN)
    {
        writeCommandInHistoryFile(command);
        return executeCommandInAChildProcess(command, commandCode);
    }
    else if ((commandCode = isInBin(command->argv[0])) == EXIT_SUCCESS)
    {
        writeCommandInHistoryFile(command);
        return executeAppInAChildProcess(command);
    }
    else if (isAlias == EXIT_FAILURE)/* default: */
    {
        printf("%s: command not found\n", command->argv[0]);
        return EXIT_FAILURE;
    }
    else{
        return EXIT_SUCCESS;
    }
}

int isProgram(char *input)
{
    if (strcmp(input, "ls") == 0)
    {
        return LS_CODE;
    }
    else if (strcmp(input, "ps") == 0)
    {
        return PS_CODE;
    }
    else if (strcmp(input, "date") == 0)
    {
        return DATE_CODE;
    }
    else
    {
        return NOT_KNOWN;
    }
}

int isBuiltin(char *input)
{
    if (strcmp(input, "cd") == 0)
    {
        return CD_CODE;
    }
    else if (strcmp(input, "pwd") == 0)
    {
        return PWD_CODE;
    }
    else if (strcmp(input, "exit") == 0)
    {
        return EXIT_CODE;
    }
    else if (strcmp(input, "echo") == 0)
    {
        return ECHO_CODE;
    }
    else
    {
        return NOT_KNOWN;
    }
}

int isInBin(char *input)
{
    return findFileInDirectory(BIN_PATH, input);
}

int replaceCommandIfIsAlias(Command *command)
{
    if (strcmp(command->argv[0], "alias") == 0)
    {   
        // create alias
        return createOrPrintAlias(command);
    }
    else{
        // find alias
        char **ptr_commandFound = calloc(1, sizeof(char *));
        char *commandFound = calloc(1, sizeof(char));
        ptr_commandFound = &commandFound;

        int returnValue = findAlias(command->argv[0], ptr_commandFound);
        if(returnValue == EXIT_SUCCESS){
            command->argv[0] = commandFound;
            *command->argc = 1;
        }
        
        return returnValue;
    }
}

int executeAppInAChildProcess(Command *command)
{
    pid_t pid = fork();

    if (pid == -1)
    {
        handleError();
        return EXIT_FAILURE;
    }
    else if (pid == 0)
    {
        char* path = malloc(strlen(BIN_PATH) + strlen(command->argv[0]));
        strcpy(path, (const char *)BIN_PATH);
        strcat(path, (const char *)command->argv[0]);

        executeBinCommandInBin(path, command);
        exit(EXIT_SUCCESS);
    }
    else
    {
        wait(NULL);
        return EXIT_SUCCESS;
    }
}

int executeBuiltIn(Command *command, int commandCode){
    switch (commandCode)
    {
        case CD_CODE:
            if (executeCd(command) == EXIT_FAILURE)
            {
                handleError();
            }
            break;
        case PWD_CODE:
            if (executePwd(command) == EXIT_FAILURE)
            {
                handleError();
            }
            break;
        case EXIT_CODE:
            if (executeExit(command) == EXIT_FAILURE)
            {
                handleError();
            }
            break;
        case ECHO_CODE:
            if (executeEcho(command) == EXIT_FAILURE)
            {
                handleError();
            }
            break;
    }
    return EXIT_SUCCESS;
}

int executeCommandInAChildProcess(Command *command, int commandCode)
{
    pid_t pid = fork();

    if (pid == -1)
    {
        handleError();
        exit(EXIT_FAILURE);
    }
    else if (pid == 0)
    {
        switch (commandCode)
        {
        case LS_CODE:
            if (executeLs(*command->argc, command->argv) == EXIT_FAILURE)
            {
                handleError();
            }
            break;
        case PS_CODE:
            if (executePs(*command->argc, command->argv) == EXIT_FAILURE)
            {
                handleError();
            }
            break;
        case DATE_CODE:
            if (executeDate(*command->argc, command->argv) == EXIT_FAILURE)
            {
                handleError();
            }
            break;
        default:
            break;
        }
        exit(EXIT_SUCCESS);
    }
    else
    {
        sleep(COMMAND_DELAY);
        wait(NULL);
        return EXIT_SUCCESS;
    }
}

void handleError(){
    printf("Error : %s\n", strerror(errno));
}

void printfResult(Command *command){
    printf("debug[interpreter.c:printResult()] :sizetab : %d\n", *command->argc);
    printf("debug[interpreter.c:printResult()] :===========  \n");

    size_t count = 0;
    while (count < *command->argc)
    {
        printf("debug[interpreter.c:printResult()] :[%ld] : %s\n", count, command->argv[count]);
        count++;
    }
}

