/**
 * \file bincommand.c
 * \brief Code portion use to scan a directory
 * \author Marion L. & David L.
 * \version 0.1
 * \date 15 novembre 2020
 */
#include "bincommand.h"

#include <dirent.h>
#include <errno.h>
#include <string.h>

int findFileInDirectory(const char *dirPath, const char *filePath){
    DIR *dir;
    struct dirent *dirp;

    char cwd[1000];
    getcwd(cwd, sizeof(cwd));
    
    dir = opendir(dirPath);
    chdir(dirPath);

    while ((dirp = readdir(dir)) != NULL)
    {
        if (dirp->d_type != 4) // is file type
        {
            if (strcmp(dirp->d_name, filePath) == 0)
            {
                chdir(cwd);
                closedir(dir);
                return EXIT_SUCCESS;
            }
        }
    }
    chdir(cwd);
    closedir(dir);

    return EXIT_FAILURE;
}

int executeBinCommandInBin(const char *commandPath, Command *command){
    int execv_result = 0;
    if((execv_result = execv(commandPath, command->argv)) < 0){
        perror(strerror(errno));
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
