#ifndef INTERPRETER_H_
#define INTERPRETER_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "../utils/typedef.h"

/**
 * \fn void printfResult(Command *command)
 * \brief Print command extract from a string
 * 
 * \param command Command to print
 * 
 */
   void printfResult(Command * command);

   /**
 * \fn int evaluateInput(char *buffer, Command *output)
 * \brief Read string (usually user input) and split it by space into output
 * 
 * \param buffer Command to read, usually user input
 * \param output Object where result is put after reading, 
 * command.argv contains tab of command split by space and command.argc contains the number of string.
 * 
 * \return return 0 if succeed
 */
   int evaluateInput(char *buffer, Command *output);

   /**
 * \fn int executeInputIfExists(Command *command)
 * \brief Evaluate command (is it a built-in, a custom command or an app) and call the corresponding function
 * to execute it
 * 
 * \param command Command to execute.
 * 
 * \return return 0 if succeed.
 */
   int executeInputIfExists(Command * command);

   /**
 * \fn int isProgram(char* input)
 * \brief Check if the user input is a Program
 * 
 * \param input The user input.
 * 
 * \return return -1 if it's not a programm or the code number of the command if it is.
 */
   int isProgram(char *input);

   /**
 * \fn int isBuiltin(char* input)
 * \brief Check if the user input is a built-in (cd...)
 * 
 * \param input The user input.
 * 
 * \return return -1 if it's not a built-in or the code number of the command if it is.
 */
   int isBuiltin(char *input);

   /**
 * \fn int isAlias(char *input);
 * \brief Check if the user input is an alias
 * 
 * \param command The user input.
 * 
 * \return return -1 if it's not a built-in or the code number of the command if it is.
 */
   int replaceCommandIfIsAlias(Command *command);

   /**
 * \fn handleError()
 * \brief Print an error message based on errno
 */
   void handleError();

   /**
 * \fn int executeCommandInAChildProcess(Command *command, int commandCode)
 * \brief Create a child process and execute command in it.
 * 
 * \param command Command to execute.
 * \param commandCode Command code of the command to execute
 * 
 * \return return 0 if succeed.
 */
   int executeCommandInAChildProcess(Command *command, int commandCode);

   /**
 * \fn int executeBuiltIn(Command *command, int commandCode)
 * \brief Execute built in command
 * 
 * \param command Command to execute.
 * \param commandCode Command code of the command to execute
 * 
 * \return return 0 if succeed.
 */
   int executeBuiltIn(Command *command, int commandCode);
   /**
 * \fn int isInBin(char *input)
 * \brief Check if the command is in /bin
 * 
 * \param input The user input
 * 
 * \return return 0 if succeed.
 */
   int isInBin(char *input);

   /**
 * \fn int executeAppInAChildProcess(Command *command)
 * \brief Execute app found in /bin in a child process
 * 
 * \param command Command to execute.
 * 
 * \return return 0 if succeed.
 */
   int executeAppInAChildProcess(Command *command);

#endif // INTERPRETER_H_