#ifndef DATE_H_
#define DATE_H_

/**
 * \fn int executeDate(int argc, char **argv)
 * \brief main equivalent of date code portion
 * 
 * \param argc number param user input
 * \param argv all user input split by space
 */
int executeDate(int argc, char **argv);

#endif // DATE_H_
