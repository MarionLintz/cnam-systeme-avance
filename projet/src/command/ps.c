/**
 * \file ps.c
 * \brief Code portion use to perform ps like command
 * \author Marion L. & David L.
 * \version 0.1
 * \date 15 novembre 2020
 */
#include "ps.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define PATH_MAX 1000
#define FORMAT "%5s %s\t%8s %s\n"

int executePs(int argc, char **argv){
    // get all process directory in /proc
    // ls /proc | grep '[0-9]'

    FILE *procDirectory, *file;
    char* tty;
    char statusFilePath[50], tty_self[256], pid[PATH_MAX], flag, cmd[56], time_s[256];
    int fd, i;
    unsigned long time, stime;

    procDirectory = popen("ls /proc | grep '[0-9]'", "r");
    if (procDirectory == NULL)
    {
        return EXIT_FAILURE;
    }

    int fd_self = open("/proc/self/fd/0", O_RDONLY);
    sprintf(tty_self, "%s", ttyname(fd_self));

    printf(FORMAT, "PID", "TTY", "TIME", "CMD");

    while (fgets(pid, PATH_MAX, procDirectory) != NULL)
    {
        pid[strcspn(pid, "\n")] = 0;
        sprintf(statusFilePath, "/proc/%s/fd/0", pid);

        fd = open(statusFilePath, O_RDONLY);
        if (fd < 0)
        {
            // file not readable
            continue;
        }
        tty = ttyname(fd);

        if (tty && strcmp(tty, tty_self) == 0)
        {
            sprintf(statusFilePath, "/proc/%s/stat", pid);
            file = fopen(statusFilePath, "r");
            fscanf(file, "%d%s%c%c%c", &i, cmd, &flag, &flag, &flag);
            cmd[strlen(cmd) - 1] = '\0';

            for (i = 0; i < 11; i++)
                fscanf(file, "%lu", &time);
            fscanf(file, "%lu", &stime);
            time = (int)((double)(time + stime) / sysconf(_SC_CLK_TCK));
            sprintf(time_s, "%02lu:%02lu:%02lu",
                    (time / 3600) % 3600, (time / 60) % 60, time % 60);

            printf(FORMAT, pid, tty + 5, time_s, cmd + 1);
            fclose(file);
        }
        close(fd);
    }

    pclose(procDirectory);
    return EXIT_SUCCESS;
}