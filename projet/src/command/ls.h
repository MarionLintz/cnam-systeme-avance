#ifndef LS_H_ 
#define LS_H_

/**
 * \fn void free_if_needed(void *to_free)
 * \brief Free object pass in param if needed
 * 
 * \param to_free object needed to be freed
 */
void free_if_needed(void *to_free);

/**
 * \fn int executeLs(int argc, char **argv)
 * \brief main equivalent of ls code portion
 * 
 * \param argc number param user input
 * \param argv all user input split by space
 */
int executeLs(int argc, char **argv);

#endif // LS_H_