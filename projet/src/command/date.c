/**
 * \file date.c
 * \brief Code portion use to perform date like command
 * \author Marion L. & David L.
 * \version 0.1
 * \date 15 novembre 2020
 */
#include "date.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int executeDate(int argc, char **argv){
    time_t T = time(NULL);
    struct tm tm = *localtime(&T);

    printf("System Date is: %02d/%02d/%04d\n", tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900);
    printf("System Time is: %02d:%02d:%02d\n", tm.tm_hour, tm.tm_min, tm.tm_sec);

    return EXIT_SUCCESS;
}