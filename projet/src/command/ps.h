#ifndef PS_H_
#define PS_H_

/**
 * \fn int executePs(int argc, char **argv)
 * \brief main equivalent of ps code portion
 * 
 * \param argc number param user input
 * \param argv all user input split by space
 */
int executePs(int argc, char **argv);

#endif // PS_H_