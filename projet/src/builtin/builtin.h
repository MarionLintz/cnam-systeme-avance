#ifndef BUILTIN_H_
#define BUILTIN_H_

#include "../utils/typedef.h"

/**
 * \fn void removeChar(char *str, char c)
 * \brief remove all occurences of a giver character from a string
 * 
 * \param str string you want to remove character from
 * \param c character you want to remove
 */
void removeChar(char *str, char c);

/**
 * \fn int executeEcho(Command *command)
 * \brief built-in used to print text in std out
 * 
 * \param command user input
 */
int executeEcho(Command *command);

/**
 * \fn int executePwd(Command *command)
 * \brief built-in used to print working directory
 * 
 * \param command user input
 */
int executePwd(Command *command);

/**
 * \fn int executeExit(Command *command)
 * \brief built-in used to exit miniSHell app
 * 
 * \param command user input
 */
int executeExit(Command *command);

/**
 * \fn int executeCd(Command *command)
 * \brief built-in used to change current directory
 * 
 * \param command user input
 */
int executeCd(Command *command);

#endif // BUILTIN_H_