/**
 * \file builtin.c
 * \brief Code portion use to perform built-in command
 * \author Marion L. & David L.
 * \version 0.1
 * \date 15 novembre 2020
 */
#include "builtin.h"

#include "../utils/utils.h"
#include <errno.h>
#include <unistd.h>
#include <signal.h>

#define PATH_MAX 1000

void removeChar(char *str, char c)
{
    char *pr = str, *pw = str;
    while (*pr)
    {
        *pw = *pr++;
        pw += (*pw != c);
    }
    *pw = '\0';
}

int executeEcho(Command *command)
{
    int count = 1;
    while(count < *command->argc){
        removeChar(command->argv[count], '"');
        removeChar(command->argv[count], '\'');
        printf("%s ", command->argv[count]);
        count++;
    }
    printf("\n");

    return EXIT_SUCCESS;
}

int executePwd(Command *command)
{
    char cwd[PATH_MAX];
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        printf("%s\n", cwd);
    }
    else
    {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

int executeExit(Command *command)
{
    printf("A bientot !\n");
    kill(getpid(), SIGTERM);
    return EXIT_SUCCESS;
}

int executeCd(Command *command)
{
    int resultChdir = EXIT_SUCCESS;
    switch(*command->argc){
        case 0:
            chdir("/");
            break;
        case 2:
            if(chdir(command->argv[1]) < 0){
                resultChdir = EXIT_FAILURE;
            }
            break;
        default:
            resultChdir = EXIT_FAILURE;
            errno = EINVAL;
            break;
    }
    
    return resultChdir;
}