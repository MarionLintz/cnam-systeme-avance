# MiniSHell

#### Génération de la documentation :
```
doxygen doc/doxygen.conf
```

#### Lancer le minishell
```
$repo/projet> make
$repo/projet> ./MyMiniShell
```

#### Mode batch
```
$repo/projet> make
$repo/projet> ./MyMiniShell ping www.google.com
```
