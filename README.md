# CNAM Systeme avancé

##### Tps et projet développés dans le cadre du module "Système d'exploitation avancé" de 2ème année du cycle d'ingénieur 

## Le projet


## Les TPS

**- TP01 : Appel système & fichiers**
```
Exercice 1 : Copie de fichiers
Exercice 2 : Print reverse
Exercice 3 : ls like 
```

**- TP02 : Appel système & processus**
```
Exercice 1 : "Fork yourself"
Exercice 2 : Redirection de flux standard
Exercice 3 : Redirection de flux via pipe
```

**- TP03a : Processus & signaux**
```
Exercice 1 : "Compteur"
Exercice 2 : Ping pong
```

**- TP03b : Processus & threads**
```
Exercice 1 : Threaded sort
```

## Utilisation

1. Se rendre dans un dossier TPX, renommer le Makefile de l'exercice correspondant.
2. Ouvrir une console, et taper la commande ```make```
3. Lancer le programme, exemple : 
```
./bin/ping_pong
```
