/**
 * \file skeleton.c
 * \brief Basic parsing options skeleton.
 * \author Pierre L. <pierre1.leroy@orange.com>
 * \version 0.1
 * \date 10 septembre 2016
 *
 * Basic parsing options skeleton exemple c file.
 */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>

#include<getopt.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <time.h>
#include <pwd.h>
#include <grp.h>


#define STDOUT 1
#define STDERR 2

#define MAX_PATH_LENGTH 4096


#define USAGE_SYNTAX " directory"
#define USAGE_PARAMS "OPTIONS:\n\
  -h, --help    : display this help\n\
"

/**
 * Procedure which displays binary usage
 * by printing on stdout all available options
 *
 * \return void
 */
void print_usage(char* bin_name)
{
  dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, USAGE_PARAMS);
}


/**
 * Procedure checks if variable must be free
 * (check: ptr != NULL)
 *
 * \param void* to_free pointer to an allocated mem
 * \see man 3 free
 * \return void
 */
void free_if_needed(void* to_free)
{
  if (to_free != NULL) free(to_free);
}


/**
 *
 * \see man 3 strndup
 * \see man 3 perror
 * \return
 */
char* dup_optarg_str()
{
  char* str = NULL;

  if (optarg != NULL)
  {
    str = strndup(optarg, MAX_PATH_LENGTH);

    // Checking if ERRNO is set
    if (str == NULL)
      perror(strerror(errno));
  }

  return str;
}


/**
 * Binary options declaration
 * (must end with {0,0,0,0})
 *
 * \see man 3 getopt_long or getopt
 * \see struct option definition
 */
static struct option binary_opts[] =
{
  { "help",    no_argument,       0, 'h' },
  { 0,         0,                 0,  0  }
};

/**
 * Binary options string
 * (linked to optionn declaration)
 *
 * \see man 3 getopt_long or getopt
 */
const char* binary_optstr = "hvi:o:";



/**
 * Binary main loop
 *
 * \return 1 if it exit successfully
 */
int main(int argc, char** argv)
{
  /**
   * Binary variables
   * (could be defined in a structure)
   */

  // Parsing options
  int opt = -1;
  int opt_idx = -1;

  while ((opt = getopt_long(argc, argv, binary_optstr, binary_opts, &opt_idx)) != -1)
  {
    switch (opt)
    {
      case 'h':
        print_usage(argv[0]);

        exit(EXIT_SUCCESS);
      default :
        break;
    }
  }

  char* path = NULL;
  if(argc == 1){
    // get current directory
    path = getenv("PWD");
  }
  else if(argc == 2){
    path = argv[1];
  }
  else{
    dprintf(STDERR, "Bad usage! See HELP [--help|-h]\n");

    // Exiting with a failure ERROR CODE (== 1)
    exit(EXIT_FAILURE);
  }

  /**
  * Checking binary requirements
  */
  if(path == NULL){
    dprintf(STDERR, "Can't find working directory\n");

    // Freeing allocated data
    free_if_needed(path);
    // Exiting with a failure ERROR CODE (== 1)
    exit(EXIT_FAILURE);
  }

  // add / if isn't to the path
  if (argc != 1 && path[strlen(path) - 1] != '/')
  {
    char *tempPath = path;
    char slash[1] = "/";
    path = malloc(strlen(path) + 1);
    strcpy(path, (const char *)tempPath);
    strcat(path, (const char *)slash);
  }

  printf("Current directory : %s\n", path);



  DIR *directory_open = NULL;
  if((directory_open = opendir((const char*)path)) == NULL){
    dprintf(STDERR, "Can't open working directory\n");

    // Freeing allocated data
    free_if_needed(directory_open);
    // Exiting with a failure ERROR CODE (== 1)
    exit(EXIT_FAILURE);
  }

  printf("%-30s %-15s %-15s %-15s %15s %-15s\n",
  "Nom","Permissions","Propriétaire","Groupe","Taille",
  "Date de création et dernière maj");

  unsigned int i = 0;
  struct dirent *ptr_directory = NULL;
  for(i = 0; (ptr_directory = readdir(directory_open)) != NULL; i++)
  {

    struct stat data;
    struct tm *time_s;
    struct passwd *pwd_s;
    struct group *grp_s;

    // concat current directory and directory name
    char *current_directory_concat = ptr_directory->d_name;

    if (argc != 1){
      current_directory_concat = malloc(strlen((const char *)path) + strlen((const char *)ptr_directory->d_name));

      strcpy(current_directory_concat, (const char *)path);
      strcat(current_directory_concat, (const char *)ptr_directory->d_name);
    }

    if (lstat(current_directory_concat, &data) == -1)
    {
      perror(strerror(errno));
      continue;
    }


    if ((time_s = localtime((const time_t *)&data.st_mtim)) == NULL)
    {
      perror(strerror(errno));
      continue;
    }

    if ((pwd_s = getpwuid(data.st_uid)) == NULL)
    {
        perror(strerror(errno));
        continue;
    }
    
    if ((grp_s = getgrgid(data.st_gid)) == NULL)
    {
        perror(strerror(errno));
        continue;
    }

    printf("%-30s %-1s%-1s%-1s%-1s%-1s%-1s%-1s%-1s%-1s%-6s %-15s %-15s %15ld %02d/%02d/%04d %02d:%02d:%02d\n",
    ptr_directory->d_name, // nom
    (S_ISDIR(data.st_mode)) ? "d" : "-", //permission
    (data.st_mode & S_IRUSR) ? "r" : "-",
    (data.st_mode & S_IWUSR) ? "w" : "-",
    (data.st_mode & S_IXUSR) ? "x" : "-",
    (data.st_mode & S_IRGRP) ? "r" : "-",
    (data.st_mode & S_IWGRP) ? "w" : "-",
    (data.st_mode & S_IXGRP) ? "x" : "-",
    (data.st_mode & S_IROTH) ? "r" : "-",
    (data.st_mode & S_IWOTH) ? "w" : "-",
    (data.st_mode & S_IXOTH) ? "x" : "-",
    (pwd_s->pw_name), //owner
    (grp_s->gr_name), //groupe
    (data.st_size), // size
    time_s->tm_mday,  // latest modification
    time_s->tm_mon + 1,
    time_s->tm_year + 1900,
    time_s->tm_hour,
    time_s->tm_min,
    time_s->tm_sec
    );
  }


  return EXIT_SUCCESS;
}
