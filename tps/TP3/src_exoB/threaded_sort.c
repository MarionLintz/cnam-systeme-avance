#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<pthread.h>


#define SIZE (int) 18
int tab[SIZE];

/**
 * L'objectif de ce code est de mettre en place un thread :
 * ===
 * - réalisant l'affichage de données issues d'une structure lambda.
 * ===
 * Schématiquement :
 * ===
 *       Père SIZE (int) 18;
 *         ^
 *         |
 * Thread1 |
 */

/**
 * Structure lambda contenant des champs basiques.
 * (Ces champs n'ont aucun intérêt à par être utilisés pour l'exemple)
 * ===
 * codeA  : entier
 * buffer : tableau de 10 caractères
 */

typedef struct extremes {
  int min = 0;
  int max = 100;
} extremes;

/**
 * Fonction exécutée par le thread
 */
void * my_thread_code(void * arg) {
  // Typage de la structure passée en paramètre
  extremes * data = (extremes *) arg;

  // Initialisation de la variable du code retour
  int * return_code = malloc(sizeof(int));
  if (return_code != NULL) {
    *return_code = 1664;
  }

  // Le code métier consiste à afficher les informations de la structure.
  printf("=~> Code executé par le thread\n");
  int iterator = 0;
  extremes extremesTab;

  for(iterator;iterator < SIZE ; iterator++){
    if (tab[iterator] > data->max)
      tab[iterator] = data->max;
    if (tab[iterator] < data->min)
      tab[iterator] = data->min;
  }



  printf("=~> Terminaison du thread\n");

  // Code retour du thread
  pthread_exit((void*)return_code);
}


int main(int argc, char** argv) {
  // Variables
  pthread_t thread_id;
  extremes my_data;
  int * my_return_code;

  my_data  = malloc(sizeof(int)*2);

  // Initialisation du thread :
  // Les options de création du thread sont celles par défaut d'ou l'utilisation du la valeur NULL.
  // On positionne le pointeur sur fonction ou "callback" de la fonction my_thread_code.
  // On position le pointeur de la structure que l'on a initialisé.
  int ret =  pthread_create(&thread_id, NULL, &my_thread_code, &my_data);

  if (ret != 0) {
    perror("Impossible de créer le thread");
  }
  else {
    *tab = {8,10,12,14,19,93,4,2,58,49,73,14,15,16,17,18,19,20}
    // Attente de terminaison du thread
    pthread_join(thread_id, (void**)&my_return_code);

    // Affichage du code retour du thread
    printf("Code retour du thread : %d\n", *my_return_code);

    free(my_return_code);
  }

  return 0;
}
