#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>

static int count = 0;
#define WAITING_FOR_MAX 3

void sigterm_handler(int sig)
{
  printf("un message de fin\n");
  exit(0);
}

void sig_handler(int sig)
{
  count++;
  printf("Je compte %d Ctrl+C\n", count);

  if (count == WAITING_FOR_MAX)
  {
    printf("Ca en fait %d, on va s'arrêter là : envoi de SIGTERM\n", WAITING_FOR_MAX);
    kill(getpid(), SIGTERM);
  }
}


int main(int argc, char** argv) {
  sigset_t sig_proc;
  struct sigaction action;

  sigemptyset(&sig_proc);
  
  action.sa_mask = sig_proc;
  action.sa_flags = 0;
  action.sa_handler = sig_handler;
  sigaction(SIGINT, &action, 0);

  sigaddset(&sig_proc, SIGINT);
  sigprocmask(SIG_SETMASK, &sig_proc, NULL);

  signal(SIGTERM, &sigterm_handler);

  while(1){
    sigfillset(&sig_proc);
    sigdelset(&sig_proc, SIGINT);
    sigsuspend(&sig_proc);
  }

  return 0;
}
