#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <errno.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <sys/mman.h>
#include <string.h>

#define SETUPS_DELAY 2
#define RETURN_SIGNAL_DELAY 1
#define SCORE_TO_WIN 5
int child_pid;
static int *scorePlayerChild;
static int *scorePlayerFather;

void sig_handler_son(int sig)
{
  srand(rand());
  int sig_to_resend = 0;
  int pass;

  switch(sig){
    case SIGUSR1:
      pass = rand() % 2;

      switch (pass)
      {
        case 0:
          // il rate la balle
          printf("o  F ---|--- P   \n");
          sig_to_resend = SIGUSR2;
          break;
        case 1:
          // il reussi la balle et la renvoie
          printf("   F o--|--- P   \n");
          sig_to_resend = SIGUSR1;
          break;
      }
      break;
    case SIGUSR2:
      // adversaire a raté la balle : +1 point
      (*scorePlayerChild)++;
      printf("Score : fils %d - père %d\n", *scorePlayerChild, *scorePlayerFather);

      if (*scorePlayerChild == SCORE_TO_WIN)
      {
        sig_to_resend = SIGTERM;
        printf("Le fils gagne %d - %d\n", *scorePlayerChild, *scorePlayerFather);
      }
      else{
        printf("   F o--|--- P   \n");
        sig_to_resend = SIGUSR1;
      }

      break;
  }

  sleep(RETURN_SIGNAL_DELAY);
  kill(getppid(), sig_to_resend);
}

void sig_handler_father(int sig)
{
  srand(rand());
  int sig_to_resend = 0;
  int pass;

  switch (sig)
  {
    case SIGUSR1:
      pass = rand() % 2;

      switch (pass)
      {
        case 0:
          // il rate la balle
          printf("   F ---|--- P   o\n");
          sig_to_resend = SIGUSR2;
          break;
        case 1:
          // il reussi la balle et la renvoie
          printf("   F ---|--o P   \n");
          sig_to_resend = SIGUSR1;
          break;
      }
      break;
    case SIGUSR2:
      // adversaire a raté la balle : +1 point
      (*scorePlayerFather)++;
      printf("Score : père %d - fils %d\n", *scorePlayerFather, *scorePlayerChild);

      if (*scorePlayerFather == SCORE_TO_WIN)
      {
        printf("Le père gagne %d - %d\n", *scorePlayerFather, *scorePlayerChild);
        kill(child_pid, SIGTERM);
        kill(getpid(), SIGTERM);
      }
      else{
        printf("   F ---|--o P   \n");
        sig_to_resend = SIGUSR1;
      }

      break;
  }

  sleep(RETURN_SIGNAL_DELAY);
  kill(child_pid, sig_to_resend);
}

int main(int argc, char** argv) {
  scorePlayerChild = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
  if (scorePlayerChild == MAP_FAILED)
  {
    perror(strerror(errno));
    exit(1);
  }
  scorePlayerFather = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
  if (scorePlayerFather == MAP_FAILED)
  {
    perror(strerror(errno));
    exit(1);
  }

  *scorePlayerChild = 0;
  *scorePlayerFather = 0;

  child_pid = fork();

  srand(time(NULL));

  if (child_pid == 0)
  {
    // CODE EXECUTED BY SON
    signal(SIGUSR1, &sig_handler_son);
    signal(SIGUSR2, &sig_handler_son);

    sleep(SETUPS_DELAY);
  }
  else{
    // CODE EXECUTED BY FATHER
    signal(SIGUSR2, &sig_handler_father);
    signal(SIGUSR1, &sig_handler_father);

    printf("   F ---|--o P   \n");
    sleep(SETUPS_DELAY);
    kill(child_pid, SIGUSR1);
  }

  while(1){
  }

  return 0;
}
