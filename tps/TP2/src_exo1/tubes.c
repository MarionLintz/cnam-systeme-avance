#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/wait.h>

/**
 * L'objectif de ce code est de mettre en place d'un processus :
 * ===
 * - générant deux processus fils de même niveau hiérarchique.
 * - réalisant une communication ascendante fils -> père via les tubes.
 * ===
 * Schématiquement :
 * ===
 *       Père
 *       ^ ^
 *       | |
 * Fils1 | | Fils2
 */
int main(int argc, char** argv) {
  int fork_son_pid = fork();
  int son_code_result = 0;

  if (fork_son_pid == 0) {
      // code executé dans le fils
      printf("PID FILS : %d\n", getpid());
      printf("PID PERE : %d\n", getppid());

      exit(getpid()%10);
  }
  else{

    // affiche pid du fils
    printf("FORK SON : %i\n", fork_son_pid);

    // attend la fin du processus fils, recupere le code de retour dans son_code_result
    wait(&son_code_result);

    printf("Code de retour du fils : %i\n", son_code_result/256);
  }
    
  return 0;
}
