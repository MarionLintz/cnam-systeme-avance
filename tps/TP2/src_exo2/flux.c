#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#define STDOUT 1
#define STDERR 2

/**
 * Procedure checks if variable must be free
 * (check: ptr != NULL)
 *
 * \param void* to_free pointer to an allocated mem
 * \see man 3 free
 * \return void
 */
void free_if_needed(void *to_free)
{
    if (to_free != NULL)
        free(to_free);
}

void print_stdout(char* bin_name, char* data){
    dprintf(STDOUT, "%s : Je suis un message et %s\n", bin_name, data);
}


int main(int argc, char **argv)
{
    char* data = NULL;

    if(argc == 1){
        dprintf(STDERR, "Bad usage! You should pass another program !\n");
        // Exiting with a failure ERROR CODE (== 1)
        free_if_needed(data);
        exit(EXIT_FAILURE);
    }
    else if (argc >= 2)
    {
        data = argv[1];
        print_stdout(argv[0], data);
    }

    if(fork() == 0){
        // CODE EXECUTE PAR LE FILS
        int file_desc = 0;

        char temp_file_name[] = "tmp/proc-exerciseXXXXXX";

        printf("PID fils : %d\n", getpid());

        // ferme descripteur de la sorite standard
        close(STDOUT);

        if ((file_desc = mkstemp(temp_file_name)) == -1)
        {
            perror("Unable to create a temp file");
            exit(EXIT_FAILURE);
        }

        dup(file_desc);

        printf("Nouveau numero descripteur : %d\n", file_desc);


        int execv_result = 0;
        if((execv_result = execv(data, argv)) == -1){
            perror("Unable to execute programm file in argument");
            exit(EXIT_FAILURE);
        }
    }
    else{
        // code execute par le pere
        printf("PID pere : %d\n", getpid());
        wait(NULL);

        printf("Un message quelconque\n");
    }

    return 0;
}
